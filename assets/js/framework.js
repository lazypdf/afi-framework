//tooltips
$(document).ready(function() {
    $(".hasTip").tooltip();
});

//messages
window.setTimeout(function() { $("#message").alert('close'); }, 7000); 

function setCookie(c_name,value,exdays) {
    var exdate=new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value=escape(value) + ((exdays===null) ? "" : "; expires="+exdate.toUTCString());
    document.cookie=c_name + "=" + c_value;
}

function getCookie(c_name) {
    var name = c_name + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name)===0) return c.substring(name.length,c.length);
    }
    return "";
}

//change remember checkbox value
function setValue() {
    if($('#remember').is(':checked')) {
		$('#remember').val(1);
		if(getCookie('dz_remember') != 1) { $("#cookieModal").modal("show"); }
    } else {
		$('#remember').val(0);
    }
}

$(document).ready(function(){
	$('.checkbox').click(function() {
		if(this.checked == true) {
			$(this).val(1);	
		} else {
			$(this).val(0);	
		}
	});
});

$(document).ready(function(){
$(".clickable").click(function() {
    	var id = $(this).attr('data-id');
	$.ajax({
	    type:'GET',
	    url: "index.php?view=cpanel&task=clientData&tmpl=raw",
	    dataType: "json",
	    data: 'clientid=' + id,
            cache: false,
	    success: function(response){
		$('#NomFiscal').html(response.NomFiscal);
		$('#NomComercial').html(response.NomComercial);
		$('#Adreca').html(response.Adreca);
		$('#Poblacio').html(response.Poblacio);
		$('#Telefon1').html(response.Telefon1);
		if(response.Telefon2 != null) {
			$('#Telefon2').html(response.Telefon2);
		}
		$('#NIF').html(response.NIF);
		$('#GPS').html(response.GPS);
		$('#saldo').html(response.saldo);
		$('#edit').css('display', '');
		$('#edit').attr('href', 'index.php?view=cpanel&layout=edit&id='+id);
		$('#clientid').html(response.NomFiscal + ' <span class="glyphicon glyphicon-chevron-down"></span>');
	    }
	});
});
$(".getpdf").click(function() {
    	var id = $(this).attr('data-id');
	$('#pdf').css('display', '');
	$('#pdf').attr('href', 'index.php?view=diposits&task=getPdf&id='+id+'&tmpl=raw');
	$('#edit').css('display', '');
	$('#edit').attr('href', 'index.php?view=diposits&layout=edit&id='+id);
});
$(".getpdf_albarans").click(function() {
    	var id = $(this).attr('data-id');
	$('#pdf').css('display', '');
	$('#pdf').attr('href', 'index.php?view=albarans&task=getPdf&id='+id+'&tmpl=raw');
	$('#edit').css('display', '');
	$('#edit').attr('href', 'index.php?view=albarans&layout=edit&id='+id);
});
$(".getpdf_factures").click(function() {
    	var id = $(this).attr('data-id');
	$('#pdf').css('display', '');
	$('#pdf').attr('href', 'index.php?view=factures&task=getPdf&id='+id+'&tmpl=raw');
	$('#edit').css('display', '');
	$('#edit').attr('href', 'index.php?view=factures&layout=edit&id='+id);
});
$(".getpdf_notes").click(function() {
    	var id = $(this).attr('data-id');
	$('#pdf').css('display', '');
	$('#pdf').attr('href', 'index.php?view=notes&task=getPdf&id='+id+'&tmpl=raw');
	$('#edit').css('display', '');
	$('#edit').attr('href', 'index.php?view=notes&layout=edit&id='+id);
});
});


//password validation
function checkPassword() {
    var pass = $("#password").val();
    var passRegex = (/^(?=.*\d)[0-9a-zA-Z]{6,}$/);
    var msg   = $( "#password" ).attr( "data-message" );
    var valid = passRegex.test(pass);
    if (!valid) {
        $('#password-field').addClass('has-error');
        $('#password-field').removeClass('has-success');
        $('#btnRegister').attr('disabled', true);
        $('#password-msg').html(msg);
        return false;
    } else {
        $('#password-field').addClass('has-success');
        $('#password-field').removeClass('has-error');
        $('#btnRegister').removeAttr('disabled');
        $('#password-msg').html('');
        return true;
    }
}

//password2 validation
function checkPasswordMatch() {
    var pass  = $("#password").val();
    var pass2 = $("#password2").val();
    var msg   = $( "#password2" ).attr( "data-message" );
    if (pass2 !== pass) {
        $('#password2-field').addClass('has-error');
        $('#password2-field').removeClass('has-success');
        $('#btnRegister').attr('disabled', true);
        $('#password2-msg').html(msg);
        return false;
    } else {
        $('#password2-field').addClass('has-success');
        $('#password2-field').removeClass('has-error');
        $('#btnRegister').removeAttr('disabled');
        $('#password2-msg').html('');
        return true;
    }
}

function checkEmailExists() {
    var email = $("#email").val();
    //check if username exists...
    $.ajax({
        type:'GET',
        dataType: 'text',
        url: 'index.php?view=register&task=checkEmail&tmpl=raw&email='+email,
        data: 'email=' + email,
        cache: false,
        success: function(data) {
            //alert(data);
            if(data == 1) {
                $('#email-field').addClass('has-success');
                $('#email-field').removeClass('has-error');
                $('#btnRegister').removeAttr('disabled');
                $('#email-msg').html('');
                return true;
            } else {
                $('#email-field').addClass('has-error');
                $('#email-field').removeClass('has-success');
                $('#btnRegister').attr('disabled', true);
                $('#email-msg').html(email+' already registered');
                return false;
            }
        }
    });
}

//email validation
function checkEmail() {
    var email = $("#email").val();
    var msg   = $("#email").attr( "data-message" );

    var emailRegex = new RegExp(/^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$/i);
    var valid = emailRegex.test(email);
    if (!valid) {
        $('#email-field').addClass('has-error');
        $('#email-field').removeClass('has-success');
        $('#btnRegister').attr('disabled', true);
        $('#email-msg').html(msg);
        return false;
    } else {
        $('#email-field').addClass('has-success');
        $('#email-field').removeClass('has-error');
        $('#btnRegister').removeAttr('disabled');
        $('#email-msg').html('');
        return true;
    }
}

function checkUsernameExists() {
    var username    = $("#username").val();
    //check if username exists...
    $.ajax({
        type:'GET',
        dataType: 'text',
        url: 'index.php?view=register&task=checkUsername&tmpl=raw&username='+username,
        data: 'username=' + username,
        cache: false,
        success: function(data) {
            //alert(data);
            if(data == 1) {
                $('#username-field').addClass('has-success');
                $('#username-field').removeClass('has-error');
                $('#btnRegister').removeAttr('disabled');
                $('#username-msg').html('');
                return true;
            } else {
                $('#username-field').addClass('has-error');
                $('#username-field').removeClass('has-success');
                $('#btnRegister').attr('disabled', true);
                $('#username-msg').html(username+' already exists in database');
                return false;
            }
        }
    });
}

//username validation
function checkUsername() {
    var username    = $("#username").val();
    var msg         = $( "#username" ).attr( "data-message" );
    //check deziro's blacklist...
    var blacklist = ['polla', 'pollas', 'culo', 'culos', 'pene', 'penes', 'follador', 'fucker', 'motherfucker', 'bitch', 'puta', 'marica', 'maricon'];
    var length = blacklist.length;
    for(var i = 0; i < length; i++) {
        if(blacklist[i] == username) { 
            $('#username-field').addClass('has-error');
            $('#username-field').removeClass('has-success');
            $('#username-msg').html(msg);
            $('#btnRegister').attr('disabled', true);
            return false; 
        }
    }
    //check if empty or min lenght...
    if(username !== "" && username.length > 2) {
        $('#username-field').addClass('has-success');
        $('#username-field').removeClass('has-error');
        $('#btnRegister').removeAttr('disabled');
        $('#username-msg').html('');
        return true;
	} else {
        $('#username-field').addClass('has-error');
        $('#username-field').removeClass('has-success');
        $('#btnRegister').attr('disabled', true);
        $('#username-msg').html(msg);
        return false;
    }
}

function validateRegister() {
    var pass    = checkPassword();
    var pass2   = checkPasswordMatch();
    var email   = checkEmail();
    var email2  = checkEmailExists();
    var user    = checkUsername();
    var user2   = checkUsernameExists();
    if(pass === true && pass2 === true && email === true && user === true) {
        return true;
    } else {
        return false;
    }
}

//plugin bootstrap minus and plus
$(document).ready(function(){
	$('.btn-minuse').on('click', function() {   
		var row = $(this).attr('data-row');
		if($(this).parent().siblings('input').val() > 0) {         
			$(this).parent().siblings('input').val(parseInt($(this).parent().siblings('input').val()) - 1);
			
			var qty  = parseInt($(this).parent().siblings('input').val());
			var brut = parseFloat($('#Preu'+row).val()) * qty;
			var ImportDte = (parseFloat(brut) * parseFloat($('#PercDte'+row).val())) / 100;
			var base = parseFloat(brut) - parseFloat(ImportDte);
			
			$('#ImportBrut'+row).val(brut);
			$('#ImportDte'+row).val(ImportDte);
			$('#Base'+row).val(base);
		}
	})
	$('.btn-pluss').on('click', function() {
		var row = $(this).attr('data-row');            
		$(this).parent().siblings('input').val(parseInt($(this).parent().siblings('input').val()) + 1);
		
			var qty  = parseInt($(this).parent().siblings('input').val());
			var brut = parseFloat($('#Preu'+row).val()) * qty;
			var ImportDte = (parseFloat(brut) * parseFloat($('#PercDte'+row).val())) / 100;
			var base = parseFloat(brut) - parseFloat(ImportDte);
			
			$('#ImportBrut'+row).val(brut);
			$('#ImportDte'+row).val(ImportDte);
			$('#Base'+row).val(base);
	})
});

