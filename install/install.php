<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Afi framework installer</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- styles -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
    <link href="installer.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet'>
    <link href='http://fonts.googleapis.com/css?family=Asap' rel='stylesheet' type='text/css'>

    <!-- Scripts -->
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script src="installer.js"></script>
</head>

<body>
<div class="container">
<div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
            <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
            <p>Step 1</p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
            <p>Step 2</p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
            <p>Step 3</p>
        </div>
    </div>
</div>
<form role="form">
    <div class="row setup-content" id="step-1">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3> Step 1 - Database setup</h3>
                <div class="form-group">
                    <label class="control-label">Database driver</label>
                    <select id="driver" name="driver" required="required" class="form-control">
                    	<option value="mysqli">MySqli</option>
                    	<option value="sqlsrv">Sqlsrv</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label">Database Host</label>
                    <input  maxlength="100" type="text" id="dbhost" required="required" class="form-control" placeholder="Enter database host..."  />
                </div>
                <div class="form-group">
                    <label class="control-label">Database Name</label>
                    <input maxlength="100" type="text" id="dbname" required="required" class="form-control" placeholder="Enter database name..." />
                </div>
		<div class="form-group">
                    <label class="control-label">Database Prefix</label>
                    <input maxlength="100" type="text" id="dbpfix" required="required" class="form-control" placeholder="Enter database prefix..." />
                </div>
		<div class="form-group">
                    <label class="control-label">Database User</label>
                    <input maxlength="100" type="text" id="dbuser" required="required" class="form-control" placeholder="Enter database user..." />
                </div>
		<div class="form-group">
                    <label class="control-label">Database Password</label>
                    <input maxlength="100" type="password" id="dbpass" required="required" class="form-control" placeholder="Enter database password..." />
                </div>
                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-2">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3> Step 2 - Administrator</h3>
		<div class="form-group">
                    <label class="control-label">Site Name</label>
                    <input maxlength="200" type="text" id="sitename" required="required" class="form-control" placeholder="Enter Site Name" />
                </div>
                <div class="form-group">
                    <label class="control-label">Admin Name</label>
                    <input maxlength="200" type="text" id="adminname" required="required" class="form-control" placeholder="Enter Administrator Name" />
                </div>
                <div class="form-group">
                    <label class="control-label">Admin Email Address</label>
                    <input maxlength="200" type="text" id="adminmail" required="required" class="form-control" placeholder="Enter Administrator email address"  />
                </div>
		<div class="form-group">
                    <label class="control-label">Admin Password</label>
                    <input maxlength="200" type="password" id="adminpass" required="required" class="form-control" placeholder="Enter Administrator password"  />
                </div>
                <button class="btn btn-primary nextBtn2 btn-lg pull-right" type="button" >Next</button>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-3">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3> Step 3</h3>
		<div>Congrats Afi framework was successfully installed, for security reasons please remove the install folder.</div>
                <button class="btn btn-success btnFinish btn-lg pull-right" type="submit">Finish!</button>
            </div>
        </div>
    </div>
</form>
</div>
</body>
