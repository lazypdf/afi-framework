<?php
/**
 * @version     1.0.0 Afi Framework $
 * @package     Afi Framework
 * @copyright   Copyright © 2014 - All rights reserved.
 * @license	    GNU/GPL
 * @author	    kim
 * @author mail kim@afi.cat
 * @website	    http://www.afi.cat
 *
*/

$response = array();

$step	= $_POST['step'];
$driver = $_POST['driver'];
$dbhost = $_POST['dbhost']; 
$dbname = $_POST['dbname']; 
$dbpfix = $_POST['dbpfix'];
$dbuser = $_POST['dbuser'];
$dbpass = $_POST['dbpass']; 

$sitename  = $_POST['sitename'];
$adminname = $_POST['adminname'];
$adminpass = $_POST['adminpass'];
$adminmail = $_POST['adminmail'];

$date  = date('Y-m-d H:i:s');
$crypt = md5($sitename.$adminname.$adminpass);
$token = uniqid();

  

if($step == 1) {

	$connect = mysql_connect($dbhost, $dbuser, $dbpass);
	
	//create database
	$query = "CREATE DATABASE $dbname";
	mysql_query($query, $connect) or die('error create table');
	mysql_close();

	//create users table
	$link = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
	// Check connection
	if (!$link) {
	    die("Connection failed: " . mysqli_connect_error());
	}
	$query = "CREATE TABLE IF NOT EXISTS ".$dbpfix."_users (
	  id int(11) NOT NULL AUTO_INCREMENT,
	  username varchar(100) NOT NULL,
	  password varchar(150) NOT NULL,
	  email varchar(150) NOT NULL,
	  registerDate datetime NOT NULL,
	  lastvisitDate datetime NOT NULL,
	  level smallint(1) NOT NULL,
	  language varchar(50) NOT NULL,
	  token varchar(150) NOT NULL,
	  block smallint(1) NOT NULL DEFAULT '1',
	  image varchar(150) NOT NULL DEFAULT 'nouser.png',
	  PRIMARY KEY (id)
	);";
	if (mysqli_query($link, $query)) {
	    echo "Table Users created successfully";
	} else {
	    echo "Error creating table: " . mysqli_error($link);
	}
	mysqli_close($link);
} 

if($step == 2) {

	//insert admin user
	$link = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
	if (!$link) {
	    die("Connection failed: " . mysqli_connect_error());
	}
	$query = "INSERT INTO ".$dbpfix."_users (id, username, password, email, registerDate, lastvisitDate, level, language, token, block, image) VALUES
	(98, '".$adminname."', '".$crypt."', '".$adminmail."', '".$date."', '0000-00-00 00:00:00', 1, 'en-gb', '".$token."', 0, 'nouser.png')";
	
	mysqli_query($link, $query);
	mysqli_close($link);

	//write config file
	$txt = "<?php\n/**\n* @version     1.0.0 Afi Framework $\n* @package     Afi Framework\n* @copyright   Copyright © 2014 - All rights reserved.\n* @license	    GNU/GPL\n* @author	    kim\n* @author mail kim@afi.cat\n* @website	    http://www.afi.cat\n*\n*/\n\ndefined('_Afi') or die('restricted access');\n\nclass Configuration {\n\n\tpublic \$site        = '';\n\tpublic \$offline     = 0;\n\tpublic \$sitename    = '".$sitename."';\n\tpublic \$description = 'site description';\n\tpublic \$email       = '".$adminmail."';\n\tpublic \$debug       = 0;\n\tpublic \$driver        = '".$driver."';\n\tpublic \$host        = '".$dbhost."';\n\tpublic \$user        = '".$dbuser."';\n\tpublic \$pass        = '".$dbpass."';\n\tpublic \$database    = '".$dbname."';\n\tpublic \$dbprefix    = '".$dbpfix."';\n\tpublic \$token_time  = 300;\n\tpublic \$template    = 'clean';\n\tpublic \$cookie      = 30;\n\tpublic \$admin_mails = 1;\n\n\tpublic \$inactive = 1000;\n}";
	file_put_contents("../classes/config.php", $txt);
}