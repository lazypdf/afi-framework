$(document).ready(function () {

	var navListItems = $('div.setup-panel div a'),
         allWells = $('.setup-content'),
         allNextBtn = $('.nextBtn');
	 allNextBtn2 = $('.nextBtn2');
	 allFinishBtn = $('.btnFinish');
    	allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allFinishBtn.click(function() {
        location.href='/index.php?view=home';
    });

    allNextBtn.click(function() {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='password']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
	    sendDatabaseData();
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    allNextBtn2.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='password']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
	    sendAdminData();
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
});

function sendDatabaseData() {

	var driver = $('#driver').val();
	var dbhost = $('#dbhost').val();
	var dbname = $('#dbname').val();
	var dbpfix = $('#dbpfix').val();
	var dbuser = $('#dbuser').val();
	var dbpass = $('#dbpass').val();

	$.post("database.php", {step: 1, driver: driver, dbhost: dbhost, dbname: dbname, dbuser: dbuser, dbpass:  dbpass, dbpfix: dbpfix}, function(data, status) {
		console.log('database success');
	});

    }

    function sendAdminData() {
    		
    	var driver = $('#driver').val();
	var dbhost = $('#dbhost').val();
	var dbname = $('#dbname').val();
	var dbpfix = $('#dbpfix').val();
	var dbuser = $('#dbuser').val();
	var dbpass = $('#dbpass').val();
	    var sitename  = $('#sitename').val();
	    var adminname = $('#adminname').val();
	    var adminpass = $('#adminpass').val();
	    var adminmail = $('#adminmail').val();
	    var dbpfix    = $('#dbpfix').val();

	    $.post("database.php", {step: 2, sitename: sitename, adminname: adminname, adminpass: adminpass, adminmail: adminmail, driver: driver, dbhost: dbhost, dbname: dbname, dbuser: dbuser, dbpass:  dbpass, dbpfix: dbpfix}, function(data, status) {
		console.log('admin success');
	    });

    }