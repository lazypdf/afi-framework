<?php
/**
 * @version     1.0.0 Afi Framework $
 * @package     Afi Framework
 * @copyright   Copyright © 2014 - All rights reserved.
 * @license	    GNU/GPL
 * @author	    kim
 * @author mail kim@afi.cat
 * @website	    http://www.afi.cat
 *
*/

defined('_Afi') or die ('restricted access');

include('../includes/model.php');

class usergroups extends model
{

	function getUsergroups() 
	{
		$db   = factory::getDatabase();

	    	$db->query('SELECT * FROM #_usergroups ORDER BY id');

		return $db->fetchObjectList();
	}

	function getUsergroupData()
	{
		$db   = factory::getDatabase();

	    	$db->query('SELECT * FROM #_usergroups WHERE id = '.$_GET['id']);

		return $db->fetchObject();
	}

	function saveUsergroup()
	{
		$app    = factory::getApplication();            
		$db     = factory::getDatabase();
		$config = factory::getConfig();

		$result = $db->insertRow('#_usergroups', $_POST);

		if($result) {
			$link = $config->site.'/admin/index.php?view=usergroups';
			$type = 'success';
			$msg  = 'El grup ha estat guardat amb exit.';
		} else {
			$link = $config->site.'/admin/index.php?view=usergroups&layout=edit';
			$type = 'danger';
			$msg  = 'Hi ha hagut un error al intentar guardar aquest grup.';
		}

		$app->setMessage($msg, $type);
                $app->redirect($link);
	}

}
