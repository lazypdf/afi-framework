<div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="index.php?view=cpanel"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="index.php?view=users"><i class="fa fa-user fa-fw"></i> Users</a>
                        </li>
			<li>
                            <a href="index.php?view=usergroups"><i class="fa fa-user fa-fw"></i> Usergroups</a>
                        </li>
			<li>
                            <a href="index.php?view=config"><i class="fa fa-cog fa-fw"></i> Configuració</a>
                        </li>
			<li>
                            <a href="index.php?view=store"><i class="fa fa-gift fa-fw"></i> Installer</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
