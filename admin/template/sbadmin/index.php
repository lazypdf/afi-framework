<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Naudó Admin</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo $config->site; ?>/admin/template/<?php echo $config->admin_template; ?>/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo $config->site; ?>/admin/template/<?php echo $config->admin_template; ?>/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo $config->site; ?>/admin/template/<?php echo $config->admin_template; ?>/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<?php echo $config->site; ?>/admin/template/<?php echo $config->admin_template; ?>/bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Datatables -->
    <link href='https://cdn.datatables.net/plug-ins/1.10.6/integration/bootstrap/3/dataTables.bootstrap.css' rel='stylesheet' type='text/css'>
    <link href='https://cdn.datatables.net/tabletools/2.2.4/css/dataTables.tableTools.css' rel='stylesheet' type='text/css'>
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>

    <!-- jQuery -->
    <script src="<?php echo $config->site; ?>/admin/template/<?php echo $config->admin_template; ?>/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo $config->site; ?>/admin/template/<?php echo $config->admin_template; ?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <script src="https://cdn.datatables.net/1.10.6/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.10.6/integration/bootstrap/3/dataTables.bootstrap.js"></script>
    <script src="https://cdn.datatables.net/tabletools/2.2.4/js/dataTables.tableTools.min.js"></script>
    <script src="<?php echo $config->site; ?>/template/<?php echo $config->template; ?>/js/bootstrap-combobox.js"></script>
    <script src="<?php echo $config->site; ?>/template/<?php echo $config->template; ?>/js/repeatable-fields.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo $config->site; ?>/admin/template/<?php echo $config->admin_template; ?>/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="<?php echo $config->site; ?>/admin/template/<?php echo $config->admin_template; ?>/bower_components/raphael/raphael-min.js"></script>
    <script src="<?php echo $config->site; ?>/admin/template/<?php echo $config->admin_template; ?>/bower_components/morrisjs/morris.min.js"></script>
    <script src="<?php echo $config->site; ?>/admin/template/<?php echo $config->admin_template; ?>/js/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo $config->site; ?>/admin/template/<?php echo $config->admin_template; ?>/dist/js/sb-admin-2.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php?view=cpanel"><?php echo $config->sitename; ?> Administració</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-eye fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li><a href="<?php echo $config->domain; ?>"><i class="fa fa-eye fa-fw"></i> View page</a></li>
                    </ul>
                    <!-- /.dropdown-alerts -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="<?php echo $config->domain; ?>/index.php?view=register&task=logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <?php echo $app->getModule('sidebar'); ?>
        </nav>

	<?php include($app->getLayout(true)); ?>

    </div>
    <!-- /#wrapper -->

</body>

</html>
