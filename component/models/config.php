<?php
/**
 * @version     1.0.0 Afi Framework $
 * @package     Afi Framework
 * @copyright   Copyright © 2014 - All rights reserved.
 * @license	    GNU/GPL
 * @author	    kim
 * @author mail kim@afi.cat
 * @website	    http://www.afi.cat
 *
*/

defined('_Afi') or die ('restricted access');

include('includes/model.php');

class config extends model
{
    function saveConfig()
    {
        if(isset($_GET['task']) && $_GET['task'] == 'saveConfig') {
            
            $app  = factory::getApplication();
            $db   = factory::getDatabase();
            $user = factory::getUser();
            $lang = factory::getLanguage();
            $app->getToken($_POST['auth_token'], $config->token_time);
            $validate = false;
    
            //validate old password...
            if($_POST['old_password'] != "") {
                $old  = $app->encryptPassword($user->name, $_POST['old_password']);
                $db->query("SELECT id FROM #_users WHERE username = ".$db->quote($user->name)." AND password = ".$db->quote($old)." AND block = 0");
                if($db->loadResult() == $user->id) {
                    $validate = true;
                }
            }
            
            $obj = new stdClass();
            $obj->email     = $_POST['email'];
            if($validate && ($_POST['password'] == $_POST['password2'])) { 
                $obj->password  = $app->encryptPassword($user->name, $_POST['password']); 
            }
            $obj->language  = $_POST['language'];
            $obj->gravatar  = $_POST['gravatar'];
            $obj->birthday  = $_POST['birthday'];
            
            $result = $db->updateRow("#_users", $obj, 'id', $user->id);
            
            if($result) {
                $app->setMessage( $lang->get('CW_SETTINGS_SAVE_SUCCESS'), 'success');
            } else {
                $app->setMessage( $lang->get('CW_SETTINGS_SAVE_ERROR'), 'danger');  
            }
            $app->redirect($config->site.'/index.php?view=config');
        }
    }
    
    /**
     * Method to upload user picture
    **/
    function upload()
    {
        if(isset($_GET['task']) && $_GET['task'] == 'upload') {
            
            $app  = factory::getApplication();
            $db   = factory::getDatabase();
            $user = factory::getUser();
            $lang = factory::getLanguage();
    
            $upload_dir = 'assets/img/uploads/';
            $allowed_ext = array('jpg','jpeg','png','gif');
    
            if(strtolower($_SERVER['REQUEST_METHOD']) != 'post') {
                $this->exit_status($lang->get('CW_SETTINGS_UPLOAD_INVALID_ERROR'));  
            }
    
            if(array_key_exists('image',$_FILES) && $_FILES['image']['error'] == 0 ) {
    
                $pic = $_FILES['image'];
        
                if(!in_array(pathinfo($pic['name'], PATHINFO_EXTENSION), $allowed_ext)){
                    $this->exit_status($lang->get('CW_SETTINGS_UPLOAD_MIME_ERROR'));  
                }
                if(move_uploaded_file($pic['tmp_name'], $upload_dir.$pic['name'])) {
                    $db->updateField("#_users", "image", $pic['name'], 'id', $user->id);
                    $this->exit_status($lang->get('CW_SETTINGS_UPLOAD_SUCCESS_ERROR'));
                }
            } else {
                $this->exit_status($lang->get('CW_SETTINGS_UPLOAD_ERROR'));
            }
        }
    }
    
    function exit_status($str){
        echo json_encode(array('status'=>$str));
        exit(0);
    }
    
    function deleteAccount()
    {
        if(isset($_GET['task']) && $_GET['task'] == 'deleteAccount') {
            
            $app  = factory::getApplication();
            $db   = factory::getDatabase();
            $user = factory::getUser();
            $lang = factory::getLanguage();
            $result = $db->deleteRow('#_users', 'id', $user->id);
            if($result) {
                $result = $db->deleteRow('#_wishes', 'userid', $user->id);
                if($result) {
                    $app->setMessage($lang->get('CW_SETTINGS_DELETE_SUCCESS'), 'success');
                } else {
                    $app->setMessage($lang->get('CW_SETTINGS_DELETE_ERROR'), 'danger');
                }
            } else {
                $app->setMessage($lang->get('CW_SETTINGS_DELETE_ERROR'), 'danger');
            }
            $app->redirect($config->site.'/index.php');
        }
    }
}