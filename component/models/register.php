<?php
/**
 * @version     1.0.0 Afi Framework $
 * @package     Afi Framework
 * @copyright   Copyright © 2014 - All rights reserved.
 * @license	    GNU/GPL
 * @author	    kim
 * @author mail kim@afi.cat
 * @website	    http://www.afi.cat
 *
*/

defined('_Afi') or die ('restricted access');

include('includes/model.php');

class register extends model
{
    /**
     * Method to check if username exists
    */
    function checkUsername() 
    {
        if(isset($_GET['task']) && $_GET['task'] == 'checkUsername') {
            $db         = factory::getDatabase();
            $username   = $_GET['username'];

            $db->query('select id from #_users where username = '.$db->quote($username));
            if($id = $db->loadResult()) {
                echo false;
            } else {
                echo true;
            }
        }
    }
    
    /**
     * Method to check if email exists
    */
    function checkEmail() 
    {
        if(isset($_GET['task']) && $_GET['task'] == 'checkEmail') {
            $db         = factory::getDatabase();
            $email      = $_GET['email'];

            $db->query('select id from #_users where email = '.$db->quote($email));
            if($id = $db->loadResult()) {
                echo false;
            } else {
                echo true;
            }
        }
    }
    
    /**
     * Method to register a new user
    */
    function register()
    {
        if(isset($_GET['task']) && $_GET['task'] == 'register') {

            $config = factory::getConfig();
            $app    = factory::getApplication();
            $db     = factory::getDatabase();
            $user   = factory::getUser();
            $lang   = factory::getLanguage();
            
            //si un campo esta vacio abortamos...
            if($_POST['username'] == "" || $_POST['email'] == "" || $_POST['password'] == "" || $_POST['password2'] == "") {
                $app->setMessage($lang->get('Rellena todos los campos por favor'), 'danger');
                $app->redirect($config->site.'/index.php?view=register');
                return false;
            }
            
            //check if username exists...
            $db->query('select id from #_users where username = '.$db->quote($_POST['username']));
            if($id = $db->loadResult()) {
                $app->setMessage($lang->get('El nombre '.$_POST['username'].' ya existe, por favor elige otro'), 'danger');
                $app->redirect($config->site.'/index.php?view=register');
                return false;
            }
            
            //check if email exists...
            $db->query('select id from #_users where email = '.$db->quote($_POST['email']));
            if($id = $db->loadResult()) {
                $app->setMessage($lang->get('El email ya existe, por favor elige otro'), 'danger');
                $app->redirect($config->site.'/index.php?view=register');
                return false;
            }
        
            $app->getToken($_POST['auth_token'], $config->token_time);
            
            if($_POST['password'] === $_POST['password2']) {
                unset($_POST['password2']);
                unset($_POST['auth_token']);
                $_POST['password']      = $app->encryptPassword($_POST['username'], $_POST['password']);
                $_POST['registerDate']  = date('Y-m-d H:i:s');
                $token                  = uniqid();
                $_POST['token']         = $token;
                $_POST['language']      = 'en-gb';
                $result = $db->insertRow('#_users', $_POST);
                if($result) {
                    //send a confirmation to the user...
                    $subject    = $lang->replace('CW_REGISTER_WELCOME_SUBJECT', $config->sitename);
                    $link       = $config->site.'/index.php?view=register&task=validate&token='.$token;
                    $body       = $lang->replace('CW_REGISTER_WELCOME_BODY', $_POST['username'],  $config->sitename, $link);
                    $send       = $this->sendMail($_POST['email'], $_POST['username'], $subject, $body);
        
                    if($send) {
                        $app->setMessage($lang->replace('CW_REGISTER_SUCCESS_MSG', $config->sitename), 'success');
                        $app->redirect($config->site.'/index.php');
                        exit(0);
                    } else {
                        //mostrar el link de activacion en el mensaje ya que fallo el email...
                        $app->setMessage($lang->replace('CW_REGISTER_EMAIL_ERROR_MSG', $link), 'danger');
                        $app->redirect($config->site.'/index.php?view=register');
                        return true;
                    }
                } else {
                    $app->setMessage($lang->get('CW_REGISTER_ERROR_MSG'), 'danger');
                    $app->redirect($config->site.'/index.php?view=register');
                    return false;
                }
            } else {
                $app->setMessage($lang->get('CW_REGISTER_PASSWORDS_NOT_MATCH_MSG'), 'danger');
                $app->redirect($config->site.'/index.php?view=register');
                return false;
            }
        }
    }
    
    /**
     * Method to reset the user password
    */
    function reset()
    {
        if(isset($_GET['task']) && $_GET['task'] == 'reset') {
        
            $config = factory::getConfig();
            $app    = factory::getApplication();
            $db     = factory::getDatabase();
            $user   = factory::getUser();
            $lang   = factory::getLanguage();
            
            //si un campo esta vacio abortamos...
            if($_POST['username'] == "" || $_POST['email'] == "") {
                $app->setMessage($lang->get('Rellena todos los campos por favor'), 'danger');
                $app->redirect($config->site.'/index.php?view=register&layout=reset');
                return false;
            }
            
            $username   = $db->quote($_POST['username']);
            $email      = $db->quote($_POST['email']);
            $secret     = $db->quote($_POST['secret']);
            $db->query("SELECT id FROM #_users WHERE email = $email AND username = $username AND token = $secret AND block = 0");
            $id = $db->loadResult();
            $newpassword = uniqid();
            $password = $app->encryptPassword($_POST['username'], $newpassword);
            $result = $db->updateField('#_users', 'password', $password, 'id', $id);
            //send email to user...
            if($result) {
                //send a confirmation to the user...
                $subject  = $lang->replace('CW_REGISTER_RESET_SUBJECT', $config->sitename);
                $body     = $lang->replace('CW_REGISTER_RESET_BODY', $_POST['username'], $config->sitename, $newpassword);
                $send     = $this->sendMail($_POST['email'], $_POST['username'], $subject, $body);
    
                if($send) {
                    $app->setMessage($lang->get('CW_REGISTER_RESET_SUCCESS_MSG'), 'success');
                    $app->redirect($config->site.'/view=register');
                } else {
                    $app->setMessage($lang->get('CW_REGISTER_RESET_ERROR_MSG'), 'danger');
                }
            } else {
                $app->setMessage($lang->get('CW_REGISTER_RESET_ERROR_MSG'), 'danger');
            }
        }
    }
       
    /**
     * Method to login into the application
    */
    function login()
    { 
        if(isset($_GET['task']) && $_GET['task'] == 'login') {
            
            $config = factory::getConfig();
            $app    = factory::getApplication();
            $db     = factory::getDatabase();
            $user   = factory::getUser();
            $lang   = factory::getLanguage();
            
            if($_SERVER["REQUEST_METHOD"] == "POST")
            {
                //si un campo esta vacio abortamos...
                if($_POST['username'] == "" || $_POST['password'] == "") {
                    $app->setMessage($lang->get('Rellena todos los campos por favor'), 'danger');
                    $app->redirect($config->site.'/index.php?view=register');
                    return false;
                }
                //check token
                $app->getToken($_POST['auth_token'], $config->token_time);
    
                $username = $db->quote($_POST['username']);
                $password = $db->quote($app->encryptPassword($_POST['username'], $_POST['password']));
                $remember  = "";
                $db->query("SELECT id FROM #_users WHERE username = $username AND password = $password AND block = 0");
                if($id = $db->loadResult()) {
                    $user->setAuth($id);
                    //if remember set cookie...
                    if($_POST['remember'] == 1) {
                        $user->setCookie();
                        //$remember = "&remember=1";
                    }
                    $db->updateField('#_users', 'lastvisitDate', $_POST['lastvisitDate'], 'id', $id);
                    $app->setMessage($lang->replace('CW_LOGIN_SUCCESS_MSG', $_POST['username']), 'success');
                    $link = $config->site.'/index.php?view=wishlist&user='.$id.'&username='.$user->username;
                } else {
                    $app->setMessage($lang->get('CW_LOGIN_ERROR_MSG'), 'danger'); 
                    $link = $config->site.'/index.php?view=register';
                }
                $app->redirect($link);
            }
        }
    }
    
    /**
     * Method to logout the application
    */
    function logout()
    {
        if(isset($_GET['task']) && $_GET['task'] == 'logout') {
            
            $config = factory::getConfig();
            $app    = factory::getApplication();
            unset($_SESSION['cw_userid']);
            $app->redirect($config->site);
        }
    }
    
    /**
     * Method to validate user for the first time into the application after a successful registration
    */
    function validate()
    {
        if(isset($_GET['task']) && $_GET['task'] == 'validate') {
            
            $config = factory::getConfig();
            $app    = factory::getApplication();
            $db     = factory::getDatabase();
            $user   = factory::getUser();
            $lang   = factory::getLanguage();
            
            //if token...
            if(isset($_GET['token'])) {
                $result = $db->updateField('#_users', 'block', 0, 'token', $_GET['token']);
                if($result) {
                    if($config->admin_mails == 1) {
                        $this->sendAdminMail('Nuevo registro en Deziro', "Un nuevo usuario se ha registrado en Deziro.");
                    }
                    $app->setMessage($lang->replace('CW_REGISTER_WELCOME_MSG_SUCCESS',  $config->sitename), 'success');
                } else {
                    $app->setMessage($lang->get('CW_REGISTER_WELCOME_MSG_ERROR'), 'danger');
                }
                $app->redirect($config->site.'/index.php');
            }
        }
    }
}