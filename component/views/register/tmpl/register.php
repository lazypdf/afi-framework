<?php
/**
 * @version     1.0.0 Afi Framework $
 * @package     Afi Framework
 * @copyright   Copyright © 2014 - All rights reserved.
 * @license	    GNU/GPL
 * @author	    kim
 * @author mail kim@afi.cat
 * @website	    http://www.afi.cat
 *
*/

defined('_Afi') or die ('restricted access');

if($user->getAuth()) {
    $app->redirect($config->site);
}

?>

<div class="wrap">
    
    <div class="container">

    <?php include('template/'.$config->template.'/message.php'); ?>

        <div class="col-md-6">
    	    <form  class="form-signin" name="register-form" id="registerForm" action="index.php?view=register&amp;task=register" method="post" onsubmit="return validateRegister();">
    	        <h2><?php echo $lang->get('CW_REGISTER_TITLE'); ?></h2>
    	        <hr>
    
        	    <!-- Username -->
        	    <?php echo $html->getTextField('register', 'username'); ?>
        	    <!-- E-mail -->
        	    <?php echo $html->getTextField('register', 'email'); ?>
        	    <!-- Password-->
        	    <?php echo $html->getTextField('register', 'password'); ?>
        	    <!-- Password2 -->
        	    <?php echo $html->getTextField('register', 'password2'); ?>
        	    <!-- Security token -->
        	    <?php echo $html->getTextField('register', 'auth_token', $app->setToken()); ?>
        	    <!-- Submit button -->
    	        <?php echo $html->getButton('register', 'submit'); ?> 
        	   
    	    </form>
	    </div>

        <div class="col-md-6">
    	    <form class="form-signin" name="login-form" id="login-form" action="<?php echo $config->site; ?>/index.php?view=register&amp;task=<?php if($request) : ?>loginAdd<?php else: ?>login<?php endif; ?>" method="post">
        		<h2><?php echo $lang->replace('CW_LOGIN_TITLE', $config->sitename); ?></h2>
        		<hr>
        		
        		<!-- Username-->
                <?php echo $html->getTextField('login', 'username'); ?>
                <!-- Password -->
                <?php echo $html->getTextField('login', 'password'); ?>
                <!-- Remember -->
                <?php echo $html->getCheckboxField('login', 'remember'); ?>
                <!-- Last visit -->
                <?php echo $html->getTextField('login', 'lastvisitDate', date('Y-m-d H:i:s')); ?>    
                <!-- Language -->
                <?php echo $html->getTextField('login', 'language', 'en-gb'); ?> 
                
                <?php if($request) : ?>
                    <!-- item data -->
        	        <?php 
        	        echo $html->getTextField('login', 'title', $_GET['title']);
        	        echo $html->getTextField('login', 'description', $_GET['description']);
        	        echo $html->getTextField('login', 'image', $_GET['image']);
        	        echo $html->getTextField('login', 'price', $_GET['price']);
        	        echo $html->getTextField('login', 'url', $_GET['url']);
        	        echo $html->getTextField('login', 'currency', $_GET['currency']);
        	        ?>
        	        <!-- end item data -->
    	        <?php endif; ?>
    	        <?php echo $html->getTextField('login', 'auth_token', $app->setToken()); ?>
    	        <?php echo $html->getButton('login', 'submit'); ?> 
    	    </form>
	        <p style="margin-top:10px;"><a href="index.php?view=register&amp;layout=reset"><i class="fa fa-question hasTip" title="reset your password"></i> <?php echo $lang->get('CW_LOGIN_FORGOT_PASSWORD'); ?></a></p>
	    </div>
    </div>
    <hr>
    
    <!-- modal cookie -->
    <div class="modal fade" id="cookieModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
             <div class="modal-content">
                 <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel"><?php echo $lang->get('CW_REGISTER_COOKIES_TITLE'); ?></h4>
                </div>
                <div class="modal-body">
                    <p><?php include(CWPATH_COMPONENT.'/views/legal/cookies.php'); ?></p>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" onclick="setCookie('dz_remember', 1);" aria-hidden="true"><?php echo $lang->get('CW_ACCEPT'); ?></button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal cookie -->
    
</div> <!-- /container -->
    
</div> <!-- /wrap -->
