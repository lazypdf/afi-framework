<?php
/**
 * @version     1.0.0 Afi Framework $
 * @package     Afi Framework
 * @copyright   Copyright © 2014 - All rights reserved.
 * @license	    GNU/GPL
 * @author	    kim
 * @author mail kim@afi.cat
 * @website	    http://www.afi.cat
 *
*/

defined('_Afi') or die ('restricted access');

?>

<div class="wrap">

    <?php include('template/'.$config->template.'/message.php'); ?>

    <div class="container">
        <div class="col-md-12">
	    <form  class="form-signin" name="register-form" id="register-form" action='<?php echo $config->site; ?>/index.php?view=register&amp;task=reset' method="post">
	        <h2><?php echo $lang->get('CW_RESET_TITLE'); ?></h2>
	        <hr>
	        <?php echo $lang->get('CW_RESET_DESC'); ?>
	        <hr>

    	    <?php echo $html->getTextField('reset', 'username'); ?>
    	    <?php echo $html->getTextField('reset', 'email'); ?>
    	    <?php echo $html->getTextField('reset', 'secret'); ?>

    	    <!-- Security token -->
    	    <input type="hidden" name="auth_token" value="<?php echo $app->setToken(); ?>" />
    	    <button onclick="this.form.submit();" id="resetBtn" class="btn btn-success">Reset</button>
    	   
	    </form>
	    </div>
	</div>