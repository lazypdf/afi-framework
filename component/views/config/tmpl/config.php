<?php
/**
 * @version     1.0.0 Afi Framework $
 * @package     Afi Framework
 * @copyright   Copyright © 2014 - All rights reserved.
 * @license	    GNU/GPL
 * @author	    kim
 * @author mail kim@afi.cat
 * @website	    http://www.afi.cat
 *
*/

defined('_Afi') or die ('restricted access');
$app->addScript('jquery.filedrop.js');
$app->addScript('uploader.js');
$app->addScript('jquery.maskedinput.min.js');
$tab = $app->getVar('tab', 'profile', 'get');
?>

<script>
$(function($){
   $("#birthday").mask("9999-99-99");
});
</script>

<div class="wrap">
    
    <div class="container">

    <?php include('template/'.$config->template.'/message.php'); ?>

    <form class="form-signin" id="settings-form" action='<?php echo $config->site; ?>/index.php?view=config&amp;task=saveConfig' method="post">
    
        <h2><?php echo $lang->get('CW_SETTINGS_TITLE'); ?></h2>
        <hr />
        
        <ul class="nav nav-tabs">
            <li <?php if($tab == 'profile') { echo "class='active'"; } ?>><a href="#profile" data-toggle="tab"><?php echo $lang->get('CW_SETTINGS_PROFILE'); ?></a></li>
            <li <?php if($tab == 'wishlist') { echo "class='active'"; } ?>><a href="#wishlist" data-toggle="tab"><?php echo $lang->get('CW_SETTINGS_WISHLIST'); ?></a></li>
            <li <?php if($tab == 'image') { echo "class='active'"; } ?>><a href="#image" data-toggle="tab"><?php echo $lang->get('CW_SETTINGS_IMAGE'); ?></a></li>
        </ul>
  
        <div id="myTabContent" class="tab-content">
            <div class="tab-pane  <?php if($tab == 'profile') { echo "active in"; } ?>" id="profile">
                <!-- E-mail -->
                <?php echo $html->getTextField('config', 'email', $user->email); ?>
    
                <!-- Password-->
                <?php echo $html->getTextField('config', 'old_password'); ?>
                
                <!-- Password-->
                <?php echo $html->getTextField('config', 'password'); ?>
           
                <!-- Password -->
                <?php echo $html->getTextField('config', 'password2'); ?>
                
            </div>
            <div class="tab-pane fade  <?php if($tab == 'wishlist') { echo "active in"; } ?>" id="wishlist">
            
                <!-- Language -->
                <?php echo $html->getListField('config', 'language', $user->language); ?>
                
                <!-- Secretkey -->
                <?php echo $html->getTextField('config', 'secretkey', $user->token); ?> 
                
                <!-- Birthday -->
                <?php $user->birthday == '0000-00-00' ? $birthday = "" : $birthday = $user->birthday; ?>
                <?php echo $html->getTextField('config', 'birthday', $birthday); ?> 
            </div>
            <div class="tab-pane fade  <?php if($tab == 'image') { echo "active in"; } ?>" id="image">
            
                <!-- Gravatar -->
                <?php echo $html->getListField('config', 'gravatar', $user->gravatar); ?>
                
                <?php if($user->gravatar == 0) : ?>
                <!-- Uploader -->
                <label class="control-label"><a>Drag &amp; drop your picture</a></label>
                <div id="dropbox">
			        <span class="message">Drop images here to upload.</span>
		        </div>
                <?php endif; ?>
                
            </div>
        </div>

        <!-- Security token -->
        <?php echo $html->getTextField('config', 'auth_token', $app->setToken()); ?> 
    	
    	<?php echo $html->getButton('config', 'submit'); ?> 
    	
    	<button data-target="#myModal" class="btn btn-success pull-right" data-toggle="modal" data-original-title="<?php echo $lang->get('CW_SETTINGS_DELETE_ACCOUNT'); ?>"><?php echo $lang->get('CW_SETTINGS_DELETE_ACCOUNT'); ?></button>
    </form>

    <!-- modal delete -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel"><?php echo $lang->get('CW_SETTINGS_DELETE_ACCOUNT_TITLE'); ?></h4>
                </div>
                <div class="modal-body">
                    <?php echo $lang->replace('CW_SETTINGS_DELETE_ACCOUNT_BODY', $config->sitename, $config->sitename); ?>
                    <input type="text" name="proceed" id="proceed" />
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><?php echo $lang->get('CW_CANCEL'); ?></button>
                    <button onclick="deleteAccount(<?php echo $user->name; ?>,<?php echo $config->site; ?>)';" class="btn btn-success" data-dismiss="modal"><?php echo $lang->get('CW_DELETE'); ?></button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal delete -->
    <hr>
    
    </div> <!-- /container -->
    
</div> <!-- /wrap -->