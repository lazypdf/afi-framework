<?php
/**
 * @version     1.0.0 Afi Framework $
 * @package     Afi Framework
 * @copyright   Copyright © 2014 - All rights reserved.
 * @license	    GNU/GPL
 * @author	    kim
 * @author mail kim@afi.cat
 * @website	    http://www.afi.cat
 *
*/

defined('_Afi') or die ('restricted access');

$app->setTitle('Config');

$app->addStylesheet('template/clean/css/bootstrap-datetimepicker.min.css');
$app->addScript('template/clean/js/moment.js');
$app->addScript('template/clean/js/bootstrap-datetimepicker.min.js');
