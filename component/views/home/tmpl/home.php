<?php
/**
 * @version     1.0.0 Afi Framework $
 * @package     Afi Framework
 * @copyright   Copyright © 2014 - All rights reserved.
 * @license	    GNU/GPL
 * @author	    kim
 * @author mail kim@afi.cat
 * @website	    http://www.afi.cat
 *
*/

defined('_Afi') or die ('restricted access');

?>

<!-- Carousel -->
<div  id="carousel-example-generic" class="carousel slide visible-md visible-lg" data-ride="carousel">
  <div class="carousel-inner">
    <div class="item active">
        <img src="<?php echo $config->site; ?>/template/<?php echo $config->template; ?>/img/carrousel/carrousel1.jpg" alt="">
        <div class="carousel-caption">
         <h1><i class="fa fa-heart-o"></i>  <?php echo $config->sitename; ?><small>Beta</small></h1>
            <p class="lead"><?php echo $lang->get('CW_BRAND_SLOGAN'); ?></p>
            <hr>
            <?php if($user->getAuth()) : ?>
                <a class="btn btn-lg btn-success" href="<?php echo $config->site; ?>/index.php?view=wishlist&amp;user=<?php echo $user->id; ?>"><?php echo $lang->get('CW_MENU_WISHLIST'); ?></a>
            <?php else : ?>
                <a class="btn btn-lg btn-success" href="<?php echo $config->site; ?>/index.php?view=register"><?php echo $lang->get('CW_REGISTER'); ?></a>
            <?php endif; ?>
      </div>
    </div>
    <div class="item">
      <img src="<?php echo $config->site; ?>/template/<?php echo $config->template; ?>/img/carrousel/carrousel2.jpg" alt="">
      <div class="container">
        <div class="carousel-caption">
          <h1><i class="fa fa-heart-o"></i>  <?php echo $config->sitename; ?><small>Beta</small></h1>
            <p class="lead"><?php echo $lang->get('CW_HOME_STORES'); ?></p>
            <hr>
            <a class="btn btn-lg btn-success" href="<?php echo $config->site; ?>/index.php?view=stores"><?php echo $lang->get('CW_GETCODE'); ?></a></p>
        </div>
      </div>
    </div>
    <div class="item">
      <img src="<?php echo $config->site; ?>/template/<?php echo $config->template; ?>/img/carrousel/carrousel3.jpg" alt="">
      <div class="container">
        <div class="carousel-caption">
          <h1><i class="fa fa-heart-o"></i>  <?php echo $config->sitename; ?><small>Beta</small></h1>
            <p class="lead"><?php echo $lang->get('CW_HOME_MOBILE'); ?></p>
            <hr>
            <a class="btn btn-lg btn-success" href="<?php echo $config->site; ?>/index.php?view=users"><?php echo $lang->get('CW_GET_APP'); ?></a></p>
        </div>
      </div>
    </div>
  </div>
  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div><!-- /.carousel -->

<div class="container" style="margin-top:10px;">
<?php include('template/'.$config->template.'/message.php'); ?>
</div>

<div class="container hidden-md hidden-lg" style="margin-top:20px;">
    <div class="col-md-12 center"><img class="bounce" src="assets/img/icons/heart64.png" width="64" height="64" alt="Deziro" /></div>
        
    <div class="col-md-12 center">
        <h1>Deziro<small>Beta</small></h1>
    </div>
</div>

<div class="container">
    <div class="col-md-6">
      <h2 class="center">Users</h2>
      <p class="center"><?php echo $lang->get('CW_HOME_USERS'); ?></p>
	  <?php if($user->getAuth()) : ?>
	  <p class="center"><a class="btn btn-lg btn-success" href="<?php echo $config->site; ?>/index.php?view=wishlist&amp;user=<?php echo $user->id; ?>"><?php echo $lang->get('CW_MENU_WISHLIST'); ?></a></p>
	  <?php else : ?>
          <p class="center"><a class="btn btn-lg btn-success" href="<?php echo $config->site; ?>/index.php?view=register"><?php echo $lang->get('CW_REGISTER'); ?></a></p>
	  <?php endif; ?>
    </div>
    <div class="col-md-6">
      <h2 class="center">Shops</h2>
      <p class="center"><?php echo $lang->get('CW_STORE_EXPLANATION_HOME'); ?></p>
      <p class="center"><a class="btn btn-lg btn-success" href="<?php echo $config->site; ?>/index.php?view=stores"><?php echo $lang->get('CW_GETCODE'); ?></a></p>
   </div>
</div>

<hr>