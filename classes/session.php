<?php
/**
 * @version     1.0.0 Afi Framework $
 * @package     Afi Framework
 * @copyright   Copyright © 2014 - All rights reserved.
 * @license	    GNU/GPL
 * @author	    kim
 * @author mail kim@afi.cat
 * @website	    http://www.afi.cat
 *
*/

defined('_Afi') or die ('restricted access');

class Session {

	public function set($name, $value) {

		$_SESSION [$name] = $value;

	}

	public function get($name) {

		if (isset ( $_SESSION [$name] ) && !empty(  $_SESSION [$name]  )) {
		return $_SESSION [$name];
		} else {
		return false;
		}
	}

	public function delete($name) {

		unset ( $_SESSION [$name] );
	}

	public function destroySession() {

		$_SESSION = array();
		session_destroy ();
	}	

}
