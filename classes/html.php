<?php
/**
 * @version     1.0.0 Afi Framework $
 * @package     Afi Framework
 * @copyright   Copyright © 2014 - All rights reserved.
 * @license	    GNU/GPL
 * @author	    kim
 * @author mail kim@afi.cat
 * @website	    http://www.afi.cat
 *
 */

defined('_Afi') or die ('restricted access');

class Html
{
    /**
     * Method to load a form
     * @param $form string the form xml name
    */
    function getForm($form)
    {
        $output = simplexml_load_file('component/forms/'.$form.'.xml'); 
        return $output;
    }
    
    /**
     * Method to render a input box
     * @param $form string the form name
     * @param $name string the field name
     * @param $default mixed optional default value
     * @return $html string a complete input field html
    */
    function getTextField($form, $name, $default='') 
    {
        $app    = factory::getApplication();
        $lang   = factory::getLanguage();
        
        $html = "";

        foreach($this->getForm($form) as $field) {
            //text inputs...
            if($field['name'] == $name) {
		$field[0]->readonly == 'true' ? $readonly = "readonly='true'" : $readonly = "";
                $field[0]->disabled == 'true' ? $disabled = "disabled='disabled'" : $disabled = "";
                $field[0]->onchange != "" ? $onchange = "onchange='".$field[0]->onchange."'" : $onchange = "";
                $field[0]->onkeyup != "" ? $onkeyup = " onkeyup='".$field[0]->onkeyup."'" : $onkeyup = "";
                if($field[0]->type != 'hidden') $html .= "<div id='".$field[0]->name."-field' class='form-group'>"; 
                if($field[0]->type != 'hidden' && $field[0]->label != "") $html .= "<label for='".$field[0]->id."'><a class='hasTip' title='".$lang->get($field[0]->placeholder)."'>".$lang->get($field[0]->label)."</a></label>";
                if($field[0]->type != 'hidden' && $field[0]->label != "") $html .= "<div class='controls'>";
                $html .= "<input type='".$field[0]->type."' id='".$field[0]->id."' value='".$default."' name='".$field[0]->name."'";
                if($field[0]->type != 'hidden') $html .= $disabled." data-message='".$lang->get($field[0]->message)."' ".$onchange.$onkeyup.$readonly." placeholder='".$lang->get($field[0]->placeholder)."' class='form-control' autocomplete='off'";
                $html .= ">";
                //if($field[0]->type != 'hidden') $html .= "<span id='".$field[0]->name."-msg'></span>";
                if($field[0]->type != 'hidden' && $field[0]->label != "") $html .= "</div>";
                if($field[0]->type != 'hidden') $html .= "</div>";
            }
        }
        return $html;
    }

    /**
     * Method to render a input box
     * @param $form string the form name
     * @param $name string the field name
     * @param $default mixed optional default value
     * @return $html string a complete input field html
    */
    function getDateField($form, $name, $default='') 
    {
        $app    = factory::getApplication();
        $lang   = factory::getLanguage();
        
        $html = "";

        foreach($this->getForm($form) as $field) {
            //text inputs...
            if($field['name'] == $name) {
		$field[0]->readonly == 'true' ? $readonly = "readonly='true'" : $readonly = "";
                $field[0]->disabled == 'true' ? $disabled = "disabled='disabled'" : $disabled = "";
                $html .= "<div id='".$field[0]->name."-field' class='form-group'>"; 
                if($field[0]->label != "") $html .= "<label for='".$field[0]->id."'><a class='hasTip' title='".$lang->get($field[0]->placeholder)."'>".$lang->get($field[0]->label)."</a></label>";
                $html .= "<div class='input-group date' id='".$field[0]->id."'>";
                $html .= "<input type='text' id='".$field[0]->id."' value='".$default."' name='".$field[0]->name."'";
                $html .= $disabled." data-message='".$lang->get($field[0]->message)."' ".$readonly." placeholder='".$lang->get($field[0]->placeholder)."' class='form-control' autocomplete='off'>";
                $html .= "<span class='input-group-addon'><span class='glyphicon glyphicon-calendar'></span></span>";
                $html .= "</div>";
                $html .= "</div>";
		$html .= "<script>$('#".$field[0]->id."').datetimepicker({viewMode: 'years',showClear: true,format: '".$field[0]->format."'});</script>";
            }
        }
        return $html;
    }

    /**
     * Method to render a input box
     * @param $form string the form name
     * @param $name string the field name
     * @param $default mixed optional default value
     * @return $html string a complete input field html
    */
    function getTextareaField($form, $name, $default='') 
    {
        $app    = factory::getApplication();
        $lang   = factory::getLanguage();
        
        $html = "";

        foreach($this->getForm($form) as $field) {
            //text inputs...
            if($field['name'] == $name) {
                $field[0]->disabled == 'true' ? $disabled = "disabled='disabled'" : $disabled = "";
                $field[0]->onchange != "" ? $onchange = "onchange='".$field[0]->onchange."'" : $onchange = "";

                $html .= "<div id='".$field[0]->name."-field' class='form-group'>"; 
                if($field[0]->label != "") $html .= "<label for='".$field[0]->id."'><a class='hasTip' title='".$lang->get($field[0]->placeholder)."'>".$lang->get($field[0]->label)."</a></label>";
                if($field[0]->label != "") $html .= "<div class='controls'>";
                $html .= "<textarea id='".$field[0]->id."' name='".$field[0]->name."' rows='".$field[0]->rows."' cols='".$field[0]->cols."' class='form-control' ".$disabled." ".$onchange.">".$default."</textarea>"; 
                //$html .= "<span id='".$field[0]->name."-msg'></span>";
                if($field[0]->label != "") $html .= "</div>";
                $html .= "</div>";
            }
        }
	
        return $html;
    }
    
    /**
     * Method to render a form button
     * @param $form string the form name
     * @param $name string the field name
     * @return $html string a complete html button
    */
    function getButton($form, $name) 
    {
        $lang   = factory::getLanguage();
        
        $html = "";

        foreach($this->getForm($form) as $field) {
            if($field['name'] == $name) {
                $field[0]->disabled == 'true' ? $disabled = "disabled='disabled'" : $disabled = "";
                $field[0]->onclick != "" ? $onclick = "onclick='".$field[0]->onclick."'" : $onclick = "";
                $field[0]->type == 'submit' ? $type = "type='".$field[0]->type."'" : $type = "";
                $html .= "<button $type id='".$field[0]->id."' ".$disabled." ".$onclick." class='btn btn-".$field[0]->color." input-".$field[0]->size."'>".$lang->get($field[0]->value)."</button>";
            }
        }
        return $html;
    }

    /**
     * Method to render a repeatable field
     * @param $form string the form name
     * @param $fields array of field names
     * @return $html string a complete repeatable field
    */
    function getRepeatable($form, $fields, $tmpl=null) 
    {
        $lang   = factory::getLanguage();
        
        $html = "<div class='repeatable'>";
		$html .= "<table class='wrapper' width='100%'>";
		$html .= "<thead><tr><td width='10%' colspan='4'><span class='add btn btn-success'><i class='fa fa-plus'></i></span></td></tr></thead>";
		$html .= "<tbody class='container'>";	

		$html .= "<tr class='template row'>";
		$html .= "<td width='10%'><div class='form-group'></div></td>";

		foreach($fields as $field) {
			foreach($this->getForm($form) as $row) {
				if($row['name'] == $field) {
				if($row[0]->type == 'text') { $html .= "<td width='10%'>".$this->getTextField($form, $field)."</td>"; }	
				if($row[0]->type == 'textarea') { $html .= "<td width='10%'>".$this->getTextareaField($form, $field)."</td>"; }	
				if($row[0]->type == 'list') { $html .= "<td width='10%'>".$this->getListField($form, $field)."</td>"; }	
				if($row[0]->type == 'checkbox') { $html .= "<td width='10%'>".$this->getCheckboxField($form, $field)."</td>"; }	
				if($row[0]->type == 'radio') { $html .= "<td width='10%'>".$this->getRadioField($form, $field)."</td>"; }	
				}
			}
		}


	$html .= '<td width="10%" align="right"><div class="form-group"><span class="remove btn btn-danger"><i class="fa fa-minus"></i></span></div></td></tr>';	

	if($tmpl != null) {
		foreach($tmpl as $item) {
			$html .= "<tr class='row fromdb'>";
			$html .= "<td width='10%'><div class='form-group'></div></td>";
			foreach($fields as $field) {
				foreach($this->getForm($form) as $row) {
				    if($row['name'] == $field) {
					if($row[0]->type == 'text') { $html .= "<td width='10%'>".$this->getTextField($form, $field, $item->$field)."</td>"; }	
					if($row[0]->type == 'textarea') { $html .= "<td width='10%'>".$this->getTextareaField($form, $field, $item->$field)."</td>"; }	
					if($row[0]->type == 'list') { $html .= "<td width='10%'>".$this->getListField($form, $field, $item->$field)."</td>"; }	
					if($row[0]->type == 'checkbox') { $html .= "<td width='10%'>".$this->getCheckboxField($form, $field, $item->$field)."</td>"; }	
					if($row[0]->type == 'radio') { $html .= "<td width='10%'>".$this->getRadioField($form, $field, $item->$field)."</td>"; }	
				    }
				}
			}
			$html .= '<td width="10%" align="right"><div class="form-group"><span class="remove btn btn-danger"><i class="fa fa-minus"></i></span></div></td></tr>';
		}
		
	} else {
		$html .= "<tr class='row'>";
		$html .= "<td width='10%'><div class='form-group'></div></td>";
		foreach($fields as $field) {
			foreach($this->getForm($form) as $row) {
			    if($row['name'] == $field) {
				if($row[0]->type == 'text') { $html .= "<td width='10%'>".$this->getTextField($form, $field)."</td>"; }	
				if($row[0]->type == 'textarea') { $html .= "<td width='10%'>".$this->getTextareaField($form, $field)."</td>"; }	
				if($row[0]->type == 'list') { $html .= "<td width='10%'>".$this->getListField($form, $field)."</td>"; }	
				if($row[0]->type == 'checkbox') { $html .= "<td width='10%'>".$this->getCheckboxField($form, $field)."</td>"; }	
				if($row[0]->type == 'radio') { $html .= "<td width='10%'>".$this->getRadioField($form, $field)."</td>"; }	
			    }
			}
		}

		$html .= '<td width="10%" align="right"><div class="form-group"><span class="remove btn btn-danger"><i class="fa fa-minus"></i></span></div></td>';
	}
	$html .= '</tr></tbody></table>';
	$html .= '</div>';
        return $html;
    }
    
    /**
     * Method to render a select box
     * @param $form string the form name
     * @param $name string the field name
     * @param $default mixed optional default value
     * @param $options array optional array of options
     * @return $html string a complete select field html
    */
    function getListField($form, $name, $default='', $options=null, $key='', $value='', $combobox=false) 
    {
        $lang   = factory::getLanguage();
        
        $html = "";

        foreach($this->getForm($form) as $field) {
            if($field['name'] == $name) {
                $field[0]->disabled == 'true' ? $disabled = "disabled='disabled'" : $disabled = "";
                $field[0]->onchange != "" ? $onchange = "onchange='".$field[0]->onchange."'" : $onchange = "";
		$combobox == true ? $class = 'combobox' : $class = '';
                $html .= "<div id='".$field[0]->name."-field' class='form-group'>";  
                if($field[0]->label != "") $html .= "<label class='control-label' for='".$field[0]->id."'><a class='hasTip' title='".$lang->get($field[0]->placeholder)."'>".$lang->get($field[0]->label)."</a></label>";
                $html .= "<select id='".$field[0]->id."' name='".$field[0]->name."' data-message='".$lang->get($field[0]->message)."' ".$onchange." class='".$class." form-control' ".$disabled.">";
		
		foreach($field[0]->option as $option) {
		      $default == $option['value'] ? $selected = "selected='selected';" : $selected = "";
		      $html .= "<option value='".$option['value']."' $selected>".$lang->get($option[0])."</option>";
		}
		
		if($options != null) {
			
			foreach($options as $option) {
				if($key == '' && $value == '') {
		            		$default == $option->$name ? $selected = "selected='selected';" : $selected = "";
		            		$html .= "<option value='".utf8_encode($option->$name)."' $selected>".utf8_encode($option->$name)."</option>";
				} else {
					$default == $option->$value ? $selected = "selected='selected';" : $selected = "";
		            		$html .= "<option value='".utf8_encode($option->$value)."' $selected>".utf8_encode($option->$key)."</option>";
				}
		        }
		}
                $html .= "</select>";
                //$html .= "<span id='".$field[0]->name."-msg'></span>";
                $html .= "</div>";
            }
        }
        return $html;
    }
    
    /**
     * Method to render a checkbox
     * @param $form string the form name
     * @param $name string the field name
     * @param $default mixed optional default value
     * @return $html string a complete checkbox field html
    */
    function getCheckboxField($form, $name, $default='') 
    {
        $lang   = factory::getLanguage();
        
        $html = "";

        foreach($this->getForm($form) as $field) {
            if($field['name'] == $name) {
                $field[0]->onclick != "" ? $onclick = "onclick='".$field[0]->onclick."'" : $onclick = "";
                $html .= "<div id='".$field[0]->name."-field' class='checkbox'>"; 
                $html .= "<label class='checkbox'>";
                foreach($field[0]->option as $option) {
                    $default == $option['value'] ? $checked = "checked='checked';" : $checked = "";
                    $html .= "<input type='checkbox' class='checkbox' name='".$field[0]->name."' id='".$field[0]->id."' value='".$option['value']."' ".$onclick."  data-message='".$lang->get($field[0]->message)."'> ".$lang->get($option[0]);
                }
                $html .= "</label>";
                //$html .= "<span id='".$field[0]->name."-msg'></span>";
                $html .= "</div>";
            }
        }
        return $html;
    }
    
    /**
     * Method to render a radio
     * @param $form string the form name
     * @param $name string the field name
     * @param $default mixed optional default value
     * @return $html string a complete radio field html
    */
    function getRadioField($form, $name, $default='')
    {
        $lang   = factory::getLanguage();
        
        $html = "";

        foreach($this->getForm($form) as $field) {
            if($field['name'] == $name) {
                $field[0]->onclick != "" ? $onclick = "onclick='".$field[0]->onclick."'" : $onclick = ""; 
		if($field[0]->label != "") $html .= "<label class='btn-group-label'><a class='hasTip' title='".$lang->get($field[0]->placeholder)."'>".$lang->get($field[0]->label)."</a></label> ";

        	//$html .= "<div class='col-sm-9'>";
                $html .= " <div class='btn-group ".$name."' data-toggle='buttons'>";
		
                foreach($field[0]->option as $option) {
                    $default == $option['value'] ? $checked = "checked='checked';" : $checked = "";
		    $default == $option['value'] ? $class = "active" : $class = "";
		    $html .= "<label class='btn btn-default ".$class."'>";
                    $html .= "<input type='radio' name='".$field[0]->name."' id='".$field[0]->id."' ".$checked." value='".$option['value']."' ".$onclick."  data-message='".$lang->get($field[0]->message)."'> ".$lang->get($option[0]);
		    $html .= "</label>";
                }

                //$html .= "</div>";
		$html .= "</div>";
            }
        }
        return $html;
    }

    function getEditable($form, $name, $url, $pk, $value)
    {
	$lang   = factory::getLanguage();
        
        $html = "";

	foreach($this->getForm($form) as $field) {
		if($field['name'] == $name) {
			$html = '<a href="#" id="'.$field[0]->id.'" data-type="'.$field[0]->type.'" data-pk="'.$pk.'" data-url="'.$url.'" data-title="'.$lang->get($field[0]->label).'">'.$lang->get($field[0]->label).'</a>';
		}
	}
	return $html;
    }
}
