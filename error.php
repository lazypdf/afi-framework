<?php
/**
 * @version     1.0.0 Afi Framework $
 * @package     Afi Framework
 * @copyright   Copyright © 2014 - All rights reserved.
 * @license	    GNU/GPL
 * @author	    kim
 * @author mail kim@afi.cat
 * @website	    http://www.afi.cat
 *
*/

session_start();
define('_Afi', 1);
// error_reporting(E_ALL ^ E_NOTICE);
date_default_timezone_set('Europe/Berlin');
define('CWPATH_BASE', dirname(__FILE__) );
define('DS', DIRECTORY_SEPARATOR );

require_once(CWPATH_BASE.DS.'includes/defines.php');
require_once(CWPATH_CLASSES.DS.'factory.php');

$config = factory::getConfig();
$app    = factory::getApplication();
$db     = factory::getDatabase();
$user   = factory::getUser();
$lang   = factory::getLanguage();
$html   = factory::getHtml();

if(isset($_SESSION['timeout']) ) {
	$session_life = time() - $_SESSION['timeout'];
	if($session_life > $config->inactive) { 
		$session->destroySession(); 
		header("Location: index.php?view=home"); 
	}
}
$_SESSION['timeout'] = time();

//set error level
ini_set('display_errors', $config->debug);

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo $config->sitename; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php echo $config->description; ?>">
    <meta name="author" content="<?php echo $config->sitename; ?>">
    <link rel="canonical" href="<?php echo $url->selfUrl(); ?>">
    <link rel="shortcut icon" href="<?php echo $config->site; ?>/assets/img/favicon.ico">

    <!-- styles -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
</head>

<div class="wrap">
    
    <div class="container">
    
        <div class="wrap">

        <div class="container">
    
          <!-- Jumbotron -->
          <div class="jumbotron center">
                <img width="128" height="128" src="assets/img/logo_small.png" class="bounce" alt="heart"/>
                <h1 class="header_error"><?php echo $lang->replace('CW_ERROR_'.$app->getErrorCode().'_HEADER', $config->sitename); ?></h1>
                <p><?php echo $lang->get('CW_ERROR_'.$app->getErrorCode().'_TEXT'); ?></p>
          </div>
    
          <hr>
    </div> <!-- /container -->
    
</div> <!-- /wrap -->
