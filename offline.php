<?php
/**
 * @version     1.0.0 Afi Framework $
 * @package     Afi Framework
 * @copyright   Copyright © 2014 - All rights reserved.
 * @license	    GNU/GPL
 * @author	    kim
 * @author mail kim@afi.cat
 * @website	    http://www.afi.cat
 *
*/

session_start();
define('_Afi', 1);
// error_reporting(E_ALL ^ E_NOTICE);
date_default_timezone_set('Europe/Berlin');
define('CWPATH_BASE', dirname(__FILE__) );
define('DS', DIRECTORY_SEPARATOR );

require_once(CWPATH_BASE.DS.'includes/defines.php');
require_once(CWPATH_CLASSES.DS.'factory.php');

$config = factory::getConfig();
$app    = factory::getApplication();
$db     = factory::getDatabase();
$user   = factory::getUser();
$lang   = factory::getLanguage();
$html   = factory::getHtml();

include('template/'.$config->template.'/head.php');
?>

<body>
<div class="wrap">
    
    <div class="container">
    
        <?php include('template/'.$config->template.'/message.php'); ?>
    
        <div class="col-md-12 center"><img class="bounce" src="assets/img/icons/heart64.png" width="64" height="64" alt="Deziro" /></div>
        
        <div class="col-md-12 center">
                <h1>Afi<small>Framework</small></h1>
        </div>
        
        <hr>

        <div class="col-md-12 center">
              <h2>Offline</h2>
        </div>

        <hr>
    
    </div> <!-- /container -->
    
</div> <!-- /wrap -->

</body>
</html>
