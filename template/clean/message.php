<?php
/**
 * @version     1.0.0 Afi Framework $
 * @package     Afi Framework
 * @copyright   Copyright © 2014 - All rights reserved.
 * @license	    GNU/GPL
 * @author	    kim
 * @author mail kim@afi.cat
 * @website	    http://www.afi.cat
 *
*/

defined('_Afi') or die ('restricted access');
if(is_dir('install')) {
	$_SESSION['message'] = 'Please remove the install directory for your security';
	$_SESSION['messageType'] = 'warning';
}
?>

 <div class="row">
    <div class="col-md-12">
        <div id="message" <?php if(isset($_SESSION['message'])) : ?>class="alert alert-<?php echo $_SESSION['messageType']; ?> alert-dismissible"<?php endif; ?>>
        <?php if(isset($_SESSION['message'])) : ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong><?php echo $_SESSION['message']; ?></strong>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php
unset($_SESSION['message']);
unset($_SESSION['messageType']);
?>
