<?php
/**
 * @version     1.0.0 Afi Framework $
 * @package     Afi Framework
 * @copyright   Copyright © 2014 - All rights reserved.
 * @license	    GNU/GPL
 * @author	    kim
 * @author mail kim@afi.cat
 * @website	    http://www.afi.cat
 *
*/

if($config->offline == 1) { $app->redirect($config->site.'/offline.php'); }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo $config->sitename; ?> - <?php echo $app->title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php echo $config->description; ?>">
    <meta name="author" content="<?php echo $config->sitename; ?>">
    <link rel="canonical" href="<?php echo $url->selfUrl(); ?>">
    <link rel="shortcut icon" href="<?php echo $config->site; ?>/assets/img/favicon.ico">

    <!-- styles -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    
    <link href="<?php echo $config->site; ?>/template/<?php echo $config->template; ?>/css/cpanel.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet'>
    <link href='http://fonts.googleapis.com/css?family=Asap' rel='stylesheet' type='text/css'>
    <link href="<?php echo $config->site; ?>/template/<?php echo $config->template; ?>/css/custom.css" rel="stylesheet">
    <?php 
if(count($app->stylesheets) > 0) : 
    foreach($app->stylesheets as $stylesheet) : ?>
        <link href="<?php echo $stylesheet; ?>" rel="stylesheet">
    <?php endforeach;
endif; ?>
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="<?php echo $config->site; ?>/template/<?php echo $config->template; ?>/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Scripts -->
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="<?php echo $config->site; ?>/assets/js/framework.js"></script>
    <?php 
if(count($app->scripts) > 0) : 
    foreach($app->scripts as $script) : ?>
        <script src="<?php echo $script; ?>"></script>
    <?php endforeach;
endif; ?>
    <script src="<?php echo $config->site; ?>/template/<?php echo $config->template; ?>/js/bootstrap-combobox.js"></script>
    <script src="<?php echo $config->site; ?>/template/<?php echo $config->template; ?>/js/repeatable-fields.js"></script>

</head>

<body>

    	<?php echo $app->getModule('topmenu'); ?>
 
        <?php include($app->getLayout()); ?>

	
<!-- FOOTER -->
<footer class="footer">

    <div class="container">
    
        <div class="col-md-4 pull-right visible-md visible-lg" style="text-align:right;"><a href="#"><?php echo $lang->get('CW_BACKTOTOP'); ?></a></div>
        <div class="col-md-6 text-muted">&copy; 2015 <?php echo $config->sitename; ?> &middot; <a href="#privacy" data-toggle="modal"><?php echo $lang->get('CW_PRIVACY'); ?></a> &middot; <a href="#terms" data-toggle="modal"><?php echo $lang->get('CW_TERMS'); ?></a></div>
        <div class="col-md-2 visible-md visible-lg"><a href="#" data-toggle="modal" data-target="#modalAfi"><i class="fa fa-cog"></i> Afi framework</a></div>
        
    </div>
    
</footer>

<!-- Privacy Modal -->
<div id="privacy" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $lang->get('CW_PRIVACY'); ?></h4>
            </div>
            <div class="modal-body">
                <p><?php include(CWPATH_COMPONENT.'/views/legal/privacy.php'); ?></p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" data-dismiss="modal" aria-hidden="true"><?php echo $lang->get('CW_CLOSE'); ?></button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Afi -->
<div class="modal fade" id="modalAfi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body center">
        <img src="assets/img/AfiFramework.png" alt="Afi framework">
	<p>Aquest es un programa de codi lliure amb llicencia Gnu/Gpl</p>
	<p>Desenvolupat per <a href="http://www.afi.cat" target="_blank">Afi informàtica</a></p> 
	<p><i class="fa fa-copyright fa-rotate-180"></i> <?php echo date('Y'); ?>
      </div>

    </div>
  </div>
</div>
<!-- Terms Modal -->
<div id="terms" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $lang->get('CW_TERMS'); ?></h4>
            </div>
            <div class="modal-body">
                <p><?php include(CWPATH_COMPONENT.'/views/legal/terms.php'); ?></p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" data-dismiss="modal" aria-hidden="true"><?php echo $lang->get('CW_CLOSE'); ?></button>
            </div>
        </div>
    </div>
</div>

  </body>
</html>
