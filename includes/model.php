<?php
/**
 * @version     1.0.0 Afi Framework $
 * @package     Afi Framework
 * @copyright   Copyright © 2014 - All rights reserved.
 * @license	    GNU/GPL
 * @author	    kim
 * @author mail kim@afi.cat
 * @website	    http://www.afi.cat
 *
*/

defined('_Afi') or die ('restricted access');

class  model
{    
    /**
     * Send email to the user
     * @param $mail string the user email
     * @param $name string the username
     * @param $subject string the mail subject
     * @param $body string the mail body
     * @return boolean true if success false if not
    */
    function sendMail($email, $name, $subject, $body)
    {
        $mail   = factory::getMailer();
        $config = factory::getConfig();
        
        $mail->setFrom($config->sitename, $config->email);
        $mail->addRecipient($name, $email);
        $mail->fillSubject($subject);
        $mail->fillMessage($body);
        if($mail->send()) {
            return true;
        }
        return false;
    }
    
    /**
     * Send email to the admin
     * @param $mail string the user email
     * @param $name string the username
     * @param $subject string the mail subject
     * @param $body string the mail body
     * @return boolean true if success false if not
    */
    function sendAdminMail($subject, $body)
    {
        $mail   = factory::getMailer();
        $config = factory::getConfig();
        
        $mail->setFrom($config->sitename, $config->email);
        $mail->addRecipient('Deziro', $config->email);
        $mail->fillSubject($subject);
        $mail->fillMessage($body);
        if($mail->send()) {
            return true;
        }
        return false;
    }
}